#include "foo.mligo"

let test_uniq_concat = 
    let xs = [1; 2; 3; 4] in
    let ys = [4; 5; 6; 7] in
    let zs = uniq_concat xs ys in
    assert (zs = [7; 6; 5; 4; 3; 2; 1])
