# ligo-pgk-mgm-demo

Demo for fetching ligo packages from npm

## To run this project

You need Ligo binary & Esy, you can install the beta using [npm](https://npmjs.com):

    % npm install -g temp-ligo-bin esy@latest

Then run the `ligo` commands from this project root to install depenencies.

    % cd ligo-main
    % ligo install
    % ligo compile contract main.mligo --project-root .
    % ligo run test main.test.mligo --project-root .

# Dependency structure

    ligo-main (esy project)
    |
    |---- ligo-foo (published on npm) [1.0.5]
    |       |
    |       |---- ligo-set-helpers (published on npm)
    |       |
    |       |---- ligo-list-helpers (published on npm) [1.0.2]
    |       
    |---- ligo-list-helpers (published on npm) [1.0.1]
    |
    |---- ligo-test_2 (published on npm) [1.0.0]
    |
    |---- ligo_test_1 (published on npm) [1.0.0]
