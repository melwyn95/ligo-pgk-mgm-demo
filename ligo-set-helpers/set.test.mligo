#include "set.mligo"

let test_union =
    let xs = Set.literal [1; 2; 3] in
    let ys = Set.literal [2; 3; 4] in
    let zs = union xs ys in
    assert (zs = (Set.literal [1; 2; 3; 4]))

let test_inter =
    let xs = Set.literal [1; 2; 3] in
    let ys = Set.literal [2; 3; 4] in
    let zs = inter xs ys in
    assert (zs = (Set.literal [2; 3]))

let test_to_list =
    let xs = Set.literal [1; 2; 3] in
    let s = to_list xs in
    assert (s = [3; 2; 1])

let test_of_list = 
    let xs = [1; 2; 3] in
    let s = of_list xs in
    assert (s = Set.literal [1; 2; 3])