
FROM node:latest AS node_base
FROM ubuntu:18.04

COPY --from=node_base . .
RUN npm install -g esy@next
RUN npm install -g temp-ligo-bin

COPY . .

WORKDIR /ligo-main

RUN ligo install
RUN ligo compile contract main.mligo --project-root .
RUN ligo run test main.test.mligo --project-root .