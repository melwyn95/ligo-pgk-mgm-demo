#import "ligo-list-helpers/list.mligo" "ListX"
#include "ligo-foo/foo.mligo"

#import "ligo_test_1/test1.mligo" "T1"
#import "ligo-test_2/test2.mligo" "T2"

type parameter =
  Increment of int
| Decrement of int
| Test

type storage = int

type return = (operation) list * storage

let concat (xs : int list) (ys : int list) = 
  ListX.concat xs ys

let zs = uniq_concat [1; 2; 3; T1.x] [2; 3; 4; T2.y]

let add (n, store : int * storage) : storage = store + n
let sub (n, store : int * storage) : storage = store - n

let main (action, store : parameter * storage) : operation list * storage =
  (([]: operation list),
   (match action with
      Increment n -> add (n, store)
    | Decrement n -> sub (n, store)
    | Test -> ListX.sum zs))