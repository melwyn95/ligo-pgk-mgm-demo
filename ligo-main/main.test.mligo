#include "main.mligo"

let test_concat = 
    let xs = [1; 2; 3] in
    let ys = [4; 5; 6] in
    let zs = concat xs ys in (* concat is from ligo-foo #include *)
    assert (zs = [1; 2; 3; 4; 5; 6])

let test_sum = 
    assert (ListX.sum zs = 76)