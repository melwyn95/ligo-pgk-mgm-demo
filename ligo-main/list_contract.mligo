#import "ligo-list-helpers/list.mligo" "XList"

type parameter =
  Concat  of int list
| Reverse

type storage = int list

type return = (operation) list * storage

let main (action, store : parameter * storage) : operation list * storage =
  (([]: operation list),
   (match action with
      Concat ys -> XList.concat store ys 
    | Reverse   -> XList.reverse store))